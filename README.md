<p align="center">
<a title="Made in India" src="https://img.shields.io/badge/MADE%20IN-INDIA-green?colorA=%23ff0000&colorB=%23017e40&style=for-the-badge"></a>
<a href="https://github.com/muneebwanee"><img title="Author" src="https://img.shields.io/badge/Author-muneeb--wanee-red.svg?style=for-the-badge&logo=github"></a>
<a href="#"><img title="Open Source" src="https://img.shields.io/badge/Open%20Source-%E2%9D%A4-green?style=for-the-badge"></a>
</p>

# LinkedIn Auto Connect Tool

Sometimes you want more `LinkedIn` connections, especially `500+`. This tool is very helpful to boost up your connections.
Just run it from your favorite terminal or command prompt and keep it running until you make enough connection requests.

> An automation tool to automate the connection requests on LinkedIn.

## Requirements

`node` >= `v1.0.0`

**Note**: If `node` and `npm` are not installed, Install them from [here](https://nodejs.org/en/download/).

## Installation

Install this tool using `npm`:

```bash
$ npm install -g ConnectIn
```

It installs two binaries: `ConnectIn` and `lac` to your system path.

## Usage

Use it as follows using `lac` command:

```bash
$ lac -u <enter_your_linkedin_email>
Enter LinkedIn password: *****
```

If you want to keep your console clean, then just pass `--no-verbose` option.

**Note**: It does not share your `LinkedIn` credentials, so it is safe to use.

## Contributing

Your PRs and stars are always welcome.

Please, try to follow:

* Clone the repository.
* Checkout `develop` branch.
* Install dependencies.
* Add your new features or fixes.
* Build the project.
* Start the scraper.


* `git clone https://github.com/muneebwanee/ConnectIn.git`
* `cd ConnectIn`
* `git checkout develop`
* `npm i`
* `npm run build`
* `npm start`

## Installation For Termux
* `$apt update`
* `$git clone https://github.com/muneebwanee/ConnectIn.git`
* `$exit` enter, And Open Termux Again
* `$npm install`
* `$npm install -g ConnectIn`
* `$lac -u <enter_your_linkedin_email>` And then it will ask for password, The enter Your LinkedIn Password (will not be shown their), Then Enter

#### >``Hence Done``

<p align="center">
<a href="#"><img title="Version" src="https://img.shields.io/badge/Version-1.0-green.svg?style=flat-square"></a>
<a href="https://github.com/muneebwanee"><img title="Followers" src="https://img.shields.io/github/followers/muneebwanee?color=blue&style=flat-square"></a>
<a href="https://twitter.com/muneebwanee"><img title="Twitter" src="https://img.shields.io/twitter/follow/muneebwanee?style=social"></a>
<a href="https://twitter.com/the_deepnet"><img title="Contributer" src="https://img.shields.io/twitter/follow/the_deepnet?label=%40the_deepnet&style=social"></a>
<a href="https://instagram.com/muneebwanee"><img title="Instagram" src="https://img.shields.io/badge/IG-%40muneebwanee-red?style=for-the-badge&logo=instagram"></a>
<a href="https://linkedin.com/in/muneebwanee"><img title="LinkedIn" src="https://img.shields.io/badge/LinkedIn%20-muneebwanee-orange?colorA=%23ff9696&colorB=%237E7B4E&style=for-the-badge"></a>
<a href="https://m.me/me.muneebwanee"><img title="Facebook" src="https://img.shields.io/badge/Chat-Messenger-blue?style=for-the-badge&logo=messenger"></a>
<a href="https://github.com/muneebwanee"><img title="GitHub" src="https://img.shields.io/badge/Github-Muneeb--Wanee-green?style=for-the-badge&logo=github"></a>
<a href="https://www.buymeacoffee.com/muneebwanee" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>
</p>


### Happy Hacking!
